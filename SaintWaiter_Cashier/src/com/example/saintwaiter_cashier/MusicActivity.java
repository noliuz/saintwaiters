package com.example.saintwaiter_cashier;

import com.example.saintwaiter_cashier.R;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.WindowManager;

public class MusicActivity extends Activity {

	MediaPlayer mMedia = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		mMedia = MediaPlayer.create(this, R.raw.call);
		mMedia.start();
	}

	 @Override
     public boolean onTouchEvent(MotionEvent event) {
		 // TODO Auto-generated method stub
		 mMedia.stop();
		 finish();
		 return super.onTouchEvent(event);
     }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music, menu);
		return true;
	}

}
