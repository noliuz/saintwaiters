package com.example.saintwaiter_svr;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button onButton,offButton;
	ListView cliLV;
	public static Handler updateUIHandler;
	TextView ipTV;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        onButton = (Button)findViewById(R.id.button1);
        offButton = (Button)findViewById(R.id.button2);
        cliLV = (ListView)findViewById(R.id.listView1);               
        ipTV = (TextView)findViewById(R.id.textView1);
        
        /*onButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	startService(new Intent(MainActivity.this, MainService.class));
            }
        });*/
        
        ipTV.setText(Utils.getLocalIpAddress());
        
        offButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	stopService(new Intent(MainActivity.this, MainService.class));
            	
            	Log.e("svr","Service stop.");
            }
        });
        
        updateUIHandler = new Handler() {
        	public void handleMessage(Message message) {
				
        		String msg = message.getData().getString("msg").toString();
				
				Log.e("svr","Handler:"+msg);
				if (msg.equals("update_client_listview")) {										
					updateClientLV();
				}
        	}
        };
        
        startService(new Intent(MainActivity.this, MainService.class));
	}
	
	public void updateClientLV() {
		ArrayAdapter<String> aa = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1);
		for (SocketClientDetail scd : MainService.al_scd) {
			String row = scd.cli_type+" "+scd.cli_name+" "/*+scd.cli_socket.toString()*/;
			aa.add(row);
		}
		cliLV.setAdapter(aa);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	if (MainService.commThread != null)
	    		MainService.commThread.interrupt();
			MainService.serverThread.interrupt();
	    	for (SocketClientDetail scd : MainService.al_scd) {
	    		try {	
	    			if (!scd.cli_socket.isClosed())
	    				scd.cli_socket.close();
	    		} catch (IOException e) {
	    			Log.e("svr",e.getMessage());
	    		}
	    	}
	    	
	    	try {
	    		MainService.serverSocket.close();
	    	} catch (IOException e) {
	    		Log.e("server socket close",e.getMessage());
	    	}
	    	
	    	stopService(new Intent(MainActivity.this, MainService.class));
	    	finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
}

