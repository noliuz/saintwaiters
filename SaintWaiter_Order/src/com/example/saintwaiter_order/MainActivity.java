package com.example.saintwaiter_order;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {

	String tablet_type = "order";
	String tablet_name = "order1";
	public static String cfg_file_path = "/saintwaiter_order.cfg";
	
	//String SERVER_IP = "10.0.2.2";
	//int SERVER_PORT = 5000;
	String SERVER_IP = "192.168.1.138";
	int SERVER_PORT = 6000;
	
	
	TabHost mTabHost;
	public static Socket socket = null;
	ClientThread ct;
	Thread tt;
	Thread cth;
	Button button1,button2,sendButton,updateGridButton,gridDeleteButton;
	parseRES pres; 
	Handler updateUIHandler;
	Spinner zonesSpin;
	Spinner tablesSpin;
	Spinner foodCatSpin;
	Spinner foodSpin,countsSpin;
	ListView addLV;
	EditText etNotice;
	TableLayout gridTL;
	boolean tablesSpinSelected=false;
	ArrayAdapter<String> aaAddLV; 
	TextView tableTV,zoneTV;
	private Dictionary<String, String> config = new Hashtable();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//create config dictionary
		String sconfig="";
		String path = "/mnt/extsd"+ cfg_file_path;		
		try {
		    sconfig = Utils.getStringFromFile(path);
		} catch (Exception e) {
			Log.e("Order",e.getMessage());
		}
		Log.e("Order",sconfig);
		String lines[] = sconfig.split("\\r?\\n");
		for (int i=0;i<lines.length;i++) {
			String tmp[] = lines[i].split("=");
			config.put(tmp[0], tmp[1]);
		}
		
		//init variable from config
		SERVER_IP = config.get("server_ip");
		SERVER_PORT = Integer.parseInt(config.get("server_port"));
		tablet_type = config.get("tablet_type");
		tablet_name = config.get("tablet_name");
		
		
		updateUIHandler = new Handler() {
			@Override
			public void handleMessage(Message message) {
				String msg = message.getData().getString("msg").toString();
				
				Log.e("Order","Handler:"+msg);
				if (msg.equals("update_zones")) {					
					Log.e("Order","In handler update zones");
					updateZoneSpinner();
				} else if (msg.equals("update_tables")) {
					updateTableSpinner();
				} else if (msg.equals("update_food_cat")) {
					updateFoodCatSpinner();
				} else if (msg.equals("update_food")) {
					updateFoodSpinner();
				} else if (msg.equals("update_orders_by_table_id")) {
					updateLocalGrid();
				} else if (msg.equals("delete_orders_by_id")) {
					updateOrdersGrid();
				} 
			}
		};
		
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		
		mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("���͡���").setContent(R.id.tab1));
		mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("���͡�����").setContent(R.id.tab2));
		mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator("��¡�����").setContent(R.id.tab3));
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
			    if(tabId.equals("tab3")) {
			        //select ��¡�������  tab 
			    	updateOrdersGrid();
			    }
			    
			}});
		
		zonesSpin = (Spinner)findViewById(R.id.spinner1);
		tablesSpin = (Spinner)findViewById(R.id.spinner2);
		foodCatSpin = (Spinner)findViewById(R.id.spinner3);
		foodSpin = (Spinner)findViewById(R.id.spinner4);
		zoneTV = (TextView)findViewById(R.id.textView5);
		tableTV = (TextView)findViewById(R.id.textView6);
		gridTL = (TableLayout)findViewById(R.id.gridTL);
		
		tablesSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				//updateTableSpinner();
		    	tablesSpinSelected = true;
		    	
		    	zoneTV.setText(zonesSpin.getSelectedItem().toString()+"/");
		    	tableTV.setText(tablesSpin.getSelectedItem().toString());
			}
			
			public void onNothingSelected(AdapterView<?> arg0) {
				tablesSpinSelected = false;
			}
		});
		
		zonesSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				updateTableSpinner();
			}
			
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		foodCatSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				updateFoodSpinner();
			}
			
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		pres = new parseRES(this);
		
		ct = new ClientThread();
		tt = new Thread(ct);
		tt.start();

		//UI
				
		aaAddLV = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1);
		countsSpin = (Spinner)findViewById(R.id.spinner5);
		etNotice = (EditText)findViewById(R.id.editText1);
		addLV = (ListView)findViewById(R.id.listView1);
		button2 = (Button) findViewById(R.id.button2);
		sendButton = (Button) findViewById(R.id.button3);
		updateGridButton = (Button) findViewById(R.id.button1);
		gridDeleteButton = (Button) findViewById(R.id.button4);
		
		gridDeleteButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	//find index to delete
            	ArrayList<Integer> al_del_index = new ArrayList<Integer>(); 
            	for (int i=0;i<gridTL.getChildCount();i++) {
            		if (i%2 != 0)
            			continue;
            		TableRow tr = (TableRow)gridTL.getChildAt(i);
            		CheckBox cb = (CheckBox)tr.getChildAt(6);
            		if (cb.isChecked()) {
            			al_del_index.add(i/2);
            		}
            	}
            	
            	if (al_del_index.isEmpty())
            		return;
            	Log.e("Order",al_del_index.toString());
            	
            	
            	//find order_id of al_del_index
            	ArrayList<String> al_del_id = new ArrayList<String>();
            	for (int j=0;j < al_del_index.size();j++) {
            		al_del_id.add(pres.order_arr.get(al_del_index.get(j)).id);            		
            	}
            	
            	//create json string
            	//CMD
            	try {
	        		ArrayList<JSONObject> ALcmd = new ArrayList<JSONObject>();
	        		JSONObject cmdObj = new JSONObject();
	        		cmdObj.put("CMD", "delete_orders_by_id");
	        		ALcmd.add(cmdObj);
	        		JSONArray JAcmd = new JSONArray(ALcmd);
	        		JSONObject JOcmd = new JSONObject();
	        		JOcmd.put("CMD", JAcmd);
	        	
	        		//ARG
	        		ArrayList<JSONObject> ALarg = new ArrayList<JSONObject>();
	        		for (int i=0;i<al_del_id.size();i++) {
	        			JSONObject argObj = new JSONObject();
	        			argObj.put("order_id", al_del_id.get(i));
	        			
	        			ALarg.add(argObj);
	        		}
	        		JSONArray JAarg = new JSONArray(ALarg);
	        		JOcmd.put("ARG", JAarg);
	        		
	        		Log.e("Order",JOcmd.toString());
	        		
	        		MainActivity.sendCMD(JOcmd.toString());
	        		
	        		updateOrdersGrid();
	        		
            	} catch (JSONException e) {
            		Log.e("Order",e.getMessage());
            	}
            }
		});
        
		
		button2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	if (foodSpin.getSelectedItem().toString().equals(""))
            		return;
            	aaAddLV.add(foodSpin.getSelectedItem().toString()+"-"+etNotice.getText().toString()+" "
            		+" :"+countsSpin.getSelectedItem().toString());
            	addLV.setAdapter(aaAddLV);
            	
            	etNotice.setText("");
            	countsSpin.setSelection(0);
            }
		});
    
		addLV.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                    int pos, long id) {
                // TODO Auto-generated method stub
            	aaAddLV.remove(aaAddLV.getItem(pos));
            	addLV.setAdapter(aaAddLV);
                return true;
            }
        });
		
		sendButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	if (!tablesSpinSelected)
            		return;
            	String table_name = tablesSpin.getSelectedItem().toString();
            	String table_id = "";
            	ArrayList<String> al_food_name = new ArrayList<String>();
            	ArrayList<String> al_food_id = new ArrayList<String>();
            	ArrayList<String> al_notice = new ArrayList<String>();
            	ArrayList<String> al_counts = new ArrayList<String>();
            	
            	//find table_id
            	for (parseRES.cTableModel tmp:pres.table_arr) {
            		if (tmp.name.equals(table_name)) {
            			table_id = tmp.id;
            		}
            	}
            	
            	//insert all data to array
            	for (int i=0;i<addLV.getCount();i++) {
            		String oneList = addLV.getItemAtPosition(i).toString();
            		String[] a_sep1 = oneList.split(":");
            		String counts = a_sep1[1].trim();
            		String[] a_sep2 = a_sep1[0].split("-");
            		String food_name = a_sep2[0];
            		String food_notice = a_sep2[1];
            		
            		al_food_name.add(food_name);
            		al_notice.add(food_notice);
            		al_counts.add(counts);
            	}
            	
            	//find food_id
            	for (int i=0;i<al_food_name.size();i++) {
            		//food_id
            		for (parseRES.cFoodModel tmp:pres.food_arr) {
            			if (tmp.name.equals(al_food_name.get(i))) {
            				al_food_id.add(tmp.id);
            				break;
            			}
            		}
            	}
            	
            	//json Encrypt
            	try {
            		//CMD
            		ArrayList<JSONObject> ALcmd = new ArrayList<JSONObject>();
            		JSONObject cmdObj = new JSONObject();
            		cmdObj.put("CMD", "submit_food");
            		ALcmd.add(cmdObj);
            		JSONArray JAcmd = new JSONArray(ALcmd);
            		JSONObject JOcmd = new JSONObject();
            		JOcmd.put("CMD", JAcmd);
            	
            		//ARG
            		ArrayList<JSONObject> ALarg = new ArrayList<JSONObject>();
            		for (int i=0;i<al_food_id.size();i++) {
            			JSONObject argObj = new JSONObject();
            			argObj.put("food_id", al_food_id.get(i));
            			argObj.put("table_id", table_id);
            			argObj.put("counts", al_counts.get(i));
            			argObj.put("notice", al_notice.get(i));
            			ALarg.add(argObj);
            		}
            		JSONArray JAarg = new JSONArray(ALarg);
            		JOcmd.put("ARG", JAarg);
            		
            		MainActivity.sendCMD(JOcmd.toString());
            	
            		//popup send dialog
            		final AlertDialog.Builder dDialog = new AlertDialog.Builder(MainActivity.this);
            		dDialog.setTitle("�����º����");
            		dDialog.setPositiveButton("���", null);
            		dDialog.show();
            		
            		aaAddLV.clear();
            		addLV.setAdapter(aaAddLV);
            		
            		Log.e("Order",JOcmd.toString());
            	} catch (JSONException e) {
            		Log.e("Order",e.getMessage());
            	}
            	
            }
		});
		
		
		updateGridButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//find table_id
				String table_id="";
				if (tablesSpin.getSelectedItem().toString().equals(""))
					return;
				
				String table_name = tablesSpin.getSelectedItem().toString();
				for (parseRES.cTableModel tmp:pres.table_arr) {
					if (tmp.name.equals(table_name)) {
						table_id = tmp.id;
					}
				}
				
				String CMD = "{ \"CMD\":[{\"CMD\":\"get_orders_by_table_id\"}],\"ARG\":[{\"table_id\":";
				CMD += "\""+table_id+"\"}]}";
				
				//Log.e("Order","updateGridCMD:"+CMD);
				MainActivity.sendCMD(CMD);
			}
		});
		
		while (socket == null);
		while (socket.isClosed());
		while (!socket.isConnected());
		
		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"report_client_detail\"}]"+
							  ",\"ARG\":[{\"type\":\""+tablet_type+"\",\"name\""+
							  ":\""+tablet_name+"\"}]}");
    	MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_zones\"}]}");
    	MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_tables\"}]}");
    	MainActivity.sendCMD("{ \"CMD\":[{\"CMD\":\"get_food_cat\"}]}");
		MainActivity.sendCMD("{ \"CMD\":[{\"CMD\":\"get_food\"}]}");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	try {
	        	socket.close();
	        	cth.interrupt();
	        	tt.interrupt();
	        } catch (IOException e) {
	        	Log.e("Order","Socket close.");
	        }
	    	finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	public void updateOrdersGrid() {
		//find table_id
		String table_id="";
		if (tablesSpin.getSelectedItem().toString().equals(""))
			return;
		
		String table_name = tablesSpin.getSelectedItem().toString();
		for (parseRES.cTableModel tmp:pres.table_arr) {
			if (tmp.name.equals(table_name)) {
				table_id = tmp.id;
			}
		}
		
		String CMD = "{ \"CMD\":[{\"CMD\":\"get_orders_by_table_id\"}],\"ARG\":[{\"table_id\":";
		CMD += "\""+table_id+"\"}]}";
		
		//Log.e("Order","updateGridCMD:"+CMD);
		MainActivity.sendCMD(CMD);
		
	}
	
	@Override
	public void onStart() {
		super.onStart();	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public static void sendCMD(String cmdJson) {
		try {			
			while (socket == null);
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(socket.getOutputStream())),
					true); 
			out.println(cmdJson);
		} catch (IOException e) {
			Log.e("Order",e.getMessage());
		}
	}
	
	class ClientThread implements Runnable {
		//public static Socket socket;

		@Override
	    public void run() {
			try {
				//tt.sleep(50);
				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
	            socket = new Socket(serverAddr, SERVER_PORT);
	            if (socket == null) {
	            	Log.e("order","socket is null");
	            } else {
	            	 CommunicationThread commth = new CommunicationThread(socket);
		             cth = new Thread(commth);
		             cth.start();
		             
		             WatchDogThread wdt = new WatchDogThread();
		     	     Thread wdtt = new Thread(wdt);
		     		 wdtt.start();
	            }
	            Log.e("Order",socket.toString());
	        } catch (UnknownHostException e1) {
	            Log.e("Order","UnknownHost Exception.");
	        } catch (IOException e1) {
	            Log.e("Order","IO Exception.",e1);
	        } /*catch (InterruptedException e1) {
	        	Log.e("Order",e1.getMessage());
	        }*/

	    }
	}
	
	class CommunicationThread implements Runnable {
        private Socket clientSocket;
        private BufferedReader input;

        public CommunicationThread(Socket sclientSocket) {
            clientSocket = sclientSocket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
            	Log.e("Order","Comm Thread error");
            	try {
            		this.clientSocket.close();
            	} catch (IOException e1) {
            		Log.e("Order","Client socket close error.");
            	}
            }
            
            Log.e("Order",clientSocket.toString());
            
        }
        
        public void run() {
            
        	
            
            if (!clientSocket.isConnected()) {
            	Log.e("Order","not connected");
            	return;
            }
            
        	while (!Thread.currentThread().isInterrupted() ) {
        		//Log.e("Order","Communication thread running");
        		try {
                    String read = "";                    
                    
                   	read = input.readLine();
                   	Log.e("Order",read);
                    if (!read.isEmpty()) {
                    	String restype = parseRES.getRESTYPE(read);
                    	Log.e("Order","restype:"+restype);
                    	
                    	if (restype.equals("zones")) {
                    		pres.get_zonesRES(read);	
                    		pres.msg = "update_zones";
                    		
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_zones");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		
                    		Log.e("Order",pres.zone_arr.get(0).name);
                    	} else if (restype.equals("tables")) {
                    		pres.get_tablesRES(read);
                    		pres.msg = "update_tables";
                    		
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_tables");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);                    		
                    		
                    		Log.e("Order",pres.table_arr.get(0).name);
                    	} else if (restype.equals("orders_by_table_id")) {
                    		//Log.e("Order",read);
                    		
                    		pres.get_orders_by_table_idRES(read);
                    		pres.msg = "orders_by_table_id";
                    		
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_orders_by_table_id");
                    		msg.setData(bd);
                    		
                    		updateUIHandler.sendMessage(msg);
                    		
                    		
                    	} else if (restype.equals("food_cat")) {                    		
                    		pres.get_food_catRES(read);
                    		pres.msg = "update_food_cat";
                    		
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_food_cat");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		
                    		Log.e("Order",pres.food_cat_arr.get(0).name);
                    	} else if (restype.equals("food")) {
                    		pres.get_foodRES(read);

                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_food");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		
                    		Log.e("Order",pres.food_arr.get(0).name);
                    	} else if (restype.equals("delete_orders_by_id")) {
                    		pres.get_foodRES(read);

                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","delete_orders_by_id");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		
                    		//Log.e("Order",pres.food_arr.get(0).name);
                    	} else if (restype.equals("ring_order_to_serve")) {
                    		Intent intent = new Intent(MainActivity.this, MusicActivity.class);
                            startActivity(intent);
                      	}
                    	
                    }
               } catch (IOException e) {
                	Log.e("Order",e.getMessage());
               } catch (NullPointerException e) {
            	  
               }
            } 
        }  
	}

	public void updateFoodCatSpinner() {
		List<String> atmp = new ArrayList<String>();
		
		for (parseRES.cFoodCatModel s : pres.food_cat_arr) {
			atmp.add(s.name);
		}
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,
				atmp);
		
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		foodCatSpin.setAdapter(aa);
	}
	
	public void updateZoneSpinner() {
		Log.e("Order","Update zones spinner.");
		
		List<String> atmp = new ArrayList<String>();
		
		for (parseRES.cZoneModel s : pres.zone_arr) {
			atmp.add(s.name);
		}
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,
				atmp);
		
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		zonesSpin.setAdapter(aa);
		
	}
	
	public void updateLocalGrid() {
		Log.e("Order","updateLocalGrid "+pres.order_arr.size());
		gridTL.removeAllViews();
		for(int i=0; i<pres.order_arr.size(); i++) {
		  TableRow row = new TableRow(this);
		
		  TextView dataCOL = new TextView(this);
	      dataCOL.setText(" ");
	      row.addView(dataCOL);
		  
		  dataCOL = new TextView(this);
	      dataCOL.setText(get_food_name_by_food_id(pres.order_arr.get(i).food_id));
	      row.addView(dataCOL);
	      
	      //blank column
	      dataCOL = new TextView(this);
	      dataCOL.setText(" ");
	      row.addView(dataCOL);
	      
	      dataCOL = new TextView(this);
	      dataCOL.setText(pres.order_arr.get(i).notice);
	      row.addView(dataCOL);
	      
	      dataCOL = new TextView(this);
	      dataCOL.setText(pres.order_arr.get(i).counts);
	      row.addView(dataCOL);
	      
	      dataCOL = new TextView(this);
	      String dt_order = pres.order_arr.get(i).dt_order;
	      String[] t_order = dt_order.split(" ");
	      String[] n_order = t_order[1].split(":");
	      String t = "   "+n_order[0]+"."+n_order[1]+"�.";
	      dataCOL.setText(t);
	      row.addView(dataCOL);
	      
	      CheckBox cbCOL = new CheckBox(this);
	      cbCOL.setChecked(false);
	      cbCOL.setText("");
	      row.addView(cbCOL);
	      
		  gridTL.addView(row);
		  
		  //add horizontal line
		  View v = new View(this);
		  v.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
		  v.setBackgroundColor(Color.rgb(51, 51, 51));
		  gridTL.addView(v);
		  
	   }
	}
	
	public String get_food_name_by_food_id(String food_id) {
		for (parseRES.cFoodModel tmp:pres.food_arr) {
			if (tmp.id.equals(food_id)) {
				return tmp.name;
			}
		}
		return "";
	}
	
	public void updateFoodSpinner() {
		//must update zoneSpinner before
		//find food_cat_id
		String food_cat_name = foodCatSpin.getSelectedItem().toString();
		String food_cat_id="";
		if (food_cat_name.isEmpty())
			return;
		
		for (int i=0;i<pres.food_cat_arr.size();i++) {
			if (pres.food_cat_arr.get(i).name.equals(food_cat_name)) {
				food_cat_id = pres.food_cat_arr.get(i).id;
				break;
			}
		}
		
		ArrayList<String> atmp = new ArrayList<String>();
		for (int i=0;i<pres.food_arr.size();i++) {
			if (pres.food_arr.get(i).food_cat_id.equals(food_cat_id))
				atmp.add(pres.food_arr.get(i).name);
		}
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_item,
				atmp);
		foodSpin.setAdapter(aa);
	}
	
	public void updateTableSpinner() {
		//must update zoneSpinner before
		//find zone_id
		String zone_name = zonesSpin.getSelectedItem().toString();
		String zone_id="";
		if (zone_name.isEmpty())
			return;
		
		for (int i=0;i<pres.zone_arr.size();i++) {
			if (pres.zone_arr.get(i).name.equals(zone_name)) {
				zone_id = pres.zone_arr.get(i).id;
				break;
			}
		}
		
		ArrayList<String> atmp = new ArrayList<String>();
		for (int i=0;i<pres.table_arr.size();i++) {
			if (pres.table_arr.get(i).zone_id.equals(zone_id))
				atmp.add(pres.table_arr.get(i).name);
		}
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_item,
				atmp);
		tablesSpin.setAdapter(aa);
	}
	
	class WatchDogThread implements Runnable {
		public WatchDogThread() {
		
		}
		
		public void run() {
		
			if (!socket.isConnected()) {
				cth.interrupt();
				tt.interrupt();
			}
		}
	}
	
	
}
