package com.example.saintwaiter_cashier;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.TextView;

public class GridTextView extends TextView{
	
	public GridTextView(Context context) {
	    super(context);
	       
    }
	
	@Override
	public void onDraw(Canvas canvas) {
		Paint p = new Paint();
		canvas.drawLine(0, getMeasuredHeight(), getMeasuredWidth(), getMeasuredHeight(), p);
		
		super.onDraw(canvas);
	    canvas.restore();
	}
}
