package com.example.saintwaiter_svr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

public class MainService extends Service {
	
	//Socket socket = null;
	public static ServerSocket serverSocket;
    public static Thread serverThread = null;
    public static Thread commThread = null;
    private TextView text;	
    public static final int SERVERPORT = 6000;
    public parseCMD pcmd;
    public static ArrayList<SocketClientDetail> al_scd; 
    
    
    @Override
	public IBinder onBind(Intent intent) {
		return null;
	}
    
	@Override
	public void onCreate() {
		super.onCreate();
		
		al_scd = new ArrayList<SocketClientDetail>();
		
		this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
        
        pcmd = new parseCMD(this);
        
        
        
        //pcmd.send_orders_to_kitchenCMD();
	}
	
	
	public void OnDestroy() {
	    super.onDestroy();
		try {
	         commThread.interrupt();
			 serverThread.interrupt();
			 serverSocket.close();
			 for (SocketClientDetail scd : al_scd) {
		        	try {
		        		if (!scd.cli_socket.isClosed())
		        			scd.cli_socket.close();
		        	} catch (IOException e) {
		        		Log.e("close_socket",e.getMessage());
		        	}
		     }
	         pcmd.closeDB();	         
	     } catch (IOException e) {
	    	 Log.e("svr","On Stop error");
	     } 
	}

	public static void sendCMD(Socket ssocket,String cmdJson) {
		try {
			while (ssocket == null);
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(ssocket.getOutputStream())),
					true); 
			out.println(cmdJson);
		} catch (IOException e) {
			Log.e("Order",e.getMessage());
		}
	}
	
	class ServerThread implements Runnable {
		public void onCreate() {
			
		}
		
		public void run() {
			try {
             	serverThread.sleep(50);
            } catch (InterruptedException e) {
            	Log.e("Order",e.getMessage());
            }
			
			try {
	            serverSocket = new ServerSocket(SERVERPORT);		
	        } catch (IOException e) {
	            Log.e("svr",e.getMessage());
	        }
			
	        while (!Thread.currentThread().isInterrupted()) {
	            try {
	            	Socket s;
	            	SocketClientDetail scd = new SocketClientDetail();
	            	s =  serverSocket.accept(); 
	            	scd.cli_socket = s;
	            	al_scd.add(scd);
	            	
	            	CommunicationThread ccommThread = new CommunicationThread(scd.cli_socket);
	                commThread = new Thread(ccommThread);
	                commThread.start();
	                Log.e("svr","CommThread Accepted.");
	            } catch (IOException e) {
	            	Log.e("svr","Server Thread error.");
	            }
	        }
	    }
	}

	class CommunicationThread implements Runnable {
        private Socket clientSocket;
        private BufferedReader input;

        public CommunicationThread(Socket sclientSocket) {
            clientSocket = sclientSocket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
            	Log.e("svr","Comm Thread error");
            	try {
            		this.clientSocket.close();
            	} catch (IOException e1) {
            		Log.e("Socket","Client socket close error.");
            	}
            }
        }
        
        public void run() {
        	 try {
             	commThread.sleep(50);
             } catch (InterruptedException e) {
             	Log.e("Order",e.getMessage());
             }
        	
        	
        	/* 
        	 if (!clientSocket.isConnected()) {        		 
        		 Log.e("CommuThread","socket not connected");
        		 for (int i=0;i<al_scd.size();i++) {                        		                         		
             		if (clientSocket.equals(al_scd.get(i).cli_socket)) {
             			al_scd.remove(i);                        			                        			                        			                        		
             		}                       		
             		break;
                 }
        		 return;
        	 }*/
             
            while (!Thread.currentThread().isInterrupted()) {
            	//delete socket client detail that not connected
            	/*
            	try {
            		PrintWriter outtest = new PrintWriter(clientSocket.getOutputStream(), true);
            		outtest.println("chk socket state");
    	        	if (outtest.checkError())
    	        		Log.e("svr","Socket closed.");
            	} catch (IOException e) {
            		Log.e("svr","Socket closed by exception.");
            	}*/
            	
            	//todo
            	try {
                   	String read = "";
                    read = input.readLine();
                    
                    if (read == null)
                    	return;
                    if (!read.isEmpty()) {
                    	String data = "";
                    	
                    	//updateConversationHandler.post(new updateUIThread(read));
                        
                    	String cmd = parseCMD.getCMD(read);
                        Log.e("svr","cmd:"+cmd);
                        if (cmd.equals("submit_food")) {
                        	pcmd.submit_foodCMD(read);
                        	Log.e("submit_food",read);
                        	
                        	//send to kitchen
                        	pcmd.force_kitchen_check_ordersCMD(al_scd);                        				            	
                        } else if (cmd.equals("report_client_detail")) {
                        	data = pcmd.get_report_client_detailCMD(read);
                        	                        	
                        	// fill al_scd with type and name
                        	String tmp[] = data.split("\\|");
                        	Log.e("svr_scd",tmp[1]);
                        	
                        	//if same scd.name delete old
                        	for (int i=0;i<al_scd.size();i++) {
                        		if (tmp[1].equals(al_scd.get(i).cli_name)) {
                        			al_scd.remove(i);
                           			break;
                        		}
                        	}                        	                        
                        	
                        	//fill al_scd
                        	for (int i=0;i<al_scd.size();i++) {                        		                         		                        		                        	
                        		if (clientSocket.equals(al_scd.get(i).cli_socket)) {                        			                        			
                        			al_scd.get(i).cli_type = tmp[0];
                        			al_scd.get(i).cli_name = tmp[1];
                        			Log.e("report_client_detail","fill al_scd."); 
                        			break;                        			                       		
                        		}                       		                        		
                        	}
                        	Log.e("report_client_detail",al_scd.get(0).cli_name+" "+al_scd.size());                        	
                        	
                        	//update client list
                        	
                        	Message bmsg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_client_listview");
                    		bmsg.setData(bd);
                    		
                        	MainActivity.updateUIHandler.sendMessage(bmsg);
                        	
                        	data = "";
                        } else if (cmd.equals("get_zones")) {
                        	data = pcmd.get_zonesCMD();
                        	Log.e("data",data);
                        } else if (cmd.equals("get_log_list")) {
                        	data = pcmd.get_log_listCMD();
                        	Log.e("data",data);
                        } else if (cmd.equals("get_tables")) {
                        	data = pcmd.get_tablesCMD();
                        	Log.e("data",data);
                        } else if (cmd.equals("get_orders_by_table_id")) {
                        	data = pcmd.get_orders_by_table_idCMD(read);
                        	Log.e("data",data);
                        } else if (cmd.equals("move_to_log_by_table_id")) {
                        	pcmd.move_to_log_by_table_idCMD(read);
                        	Log.e("data",data);                                                
                        } else if (cmd.equals("delete_orders_by_id")) {
                        	pcmd.delete_orders_by_idCMD(read);
                        	Log.e("data","delete success.");
                        } else if (cmd.equals("get_food_cat")) {
                        	data = pcmd.get_food_catCMD();
                        	Log.e("data",data);
                        } else if (cmd.equals("get_food")) {
                        	data = pcmd.get_foodCMD();
                        	Log.e("data",data);
                        } else if (cmd.equals("get_orders_for_cook")) {
                        	Log.e("get_orders_for_cook","come in");
                        	data = pcmd.get_orders_for_cookCMD(clientSocket,al_scd);
                        	Log.e("data",data);
                        } else if (cmd.equals("send_cooked_orders")) {                        	
                        	pcmd.send_cooked_ordersCMD(read);
                        	Log.e("data",data);
                        	data = "";
                        } else if (cmd.equals("ring_order_tablet")) {
                        	Log.e("ring_order_tablet","come in");
                        	pcmd.ring_order_tabletCMD(al_scd);
                        	//Log.e("data",data);
                        } else if (cmd.equals("get_orders_log")) {
                        	Log.e("ring_order_tablet","come in");
                        	data = pcmd.get_orders_logCMD(read);
                        }
                        
                        if (!data.isEmpty()) {
                        	Log.e("svr","println output");
                        	PrintWriter out = new PrintWriter(new BufferedWriter(
		            				new OutputStreamWriter(clientSocket.getOutputStream())),
		            				true);
			            	out.println(data);
                        }
                    }
                } catch (IOException e) {
                    Log.e("svr",e.getMessage());
                } catch (NullPointerException e) {
                	Log.e("svr",e.getMessage());
                }
            }
        }		 
    }

	
	
	
}
