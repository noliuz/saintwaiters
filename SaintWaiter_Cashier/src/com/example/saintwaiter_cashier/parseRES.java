package com.example.saintwaiter_cashier;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class parseRES {
	
	private static final long serialVersionUID = 12345;
	
	public class cZoneModel {
		String id;
		String name;
	}

	public class cTableModel {
		String id;
		String name;
		String zone_id;
	}

	public class cOrderModel {
		String id;
		String food_id;
		String notice;
		String dt_order;
		String dt_cooked;
		String counts;
	}
	
	public class cFoodCatModel {
		String id;
		String name;
	}
	
	public class cFoodModel {
		String id;
		String food_cat_id;
		String kitchen_id;
		String name;
		String price;
	}
	
	public class cLogListModel {
		String dt_log;
		String table_id;
	}
	
	Context context;
	
	public ArrayList<cZoneModel> zone_arr= new ArrayList<cZoneModel>(); 
	public ArrayList<cTableModel> table_arr = new ArrayList<cTableModel>();
	public ArrayList<cOrderModel> order_arr = new ArrayList<cOrderModel>();
	public ArrayList<cFoodCatModel> food_cat_arr = new ArrayList<cFoodCatModel>();
	public ArrayList<cFoodModel> food_arr = new ArrayList<cFoodModel>();
	public ArrayList<cLogListModel> log_list_arr = new ArrayList<cLogListModel>();
	
	public String msg = "";
	
	
	public parseRES(Context scontext) {
		context = scontext;
	}
	
	public static String getRESTYPE(String jsonStr) {
		String restype="";
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(0);
			restype = jo.getString("RESTYPE");	
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		}		
		return restype;
	}
	
	public void get_zonesRES(String jsonStr) {
		zone_arr = new ArrayList<cZoneModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cZoneModel zm = new cZoneModel();
				zm.id = c.getString("id");
				zm.name = c.getString("name");
				zone_arr.add(zm);
			}		
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
	
	public void get_food_catRES(String jsonStr) {
		food_cat_arr = new ArrayList<cFoodCatModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cFoodCatModel fcm = new cFoodCatModel();
				fcm.id = c.getString("id");
				fcm.name = c.getString("name");
				food_cat_arr.add(fcm);
			}		
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
	
	public void get_log_listRES(String jsonStr) {
		log_list_arr = new ArrayList<cLogListModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cLogListModel llm = new cLogListModel();
				llm.dt_log = c.getString("dt_log");
				llm.table_id = c.getString("table_id");
				log_list_arr.add(llm);
			}		
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
	
	public void get_foodRES(String jsonStr) {
		food_arr = new ArrayList<cFoodModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cFoodModel fm = new cFoodModel();
				fm.id = c.getString("id");
				fm.name = c.getString("name");
				fm.kitchen_id = c.getString("kitchen_id");
				fm.food_cat_id = c.getString("food_cat_id");
				fm.price = c.getString("price");
				food_arr.add(fm);
			}		
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
	
	public void get_tablesRES(String jsonStr) {
		table_arr = new ArrayList<cTableModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1);
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cTableModel tm = new cTableModel();
				tm.id = c.getString("id");
				tm.name = c.getString("name");
				tm.zone_id = c.getString("zone_id");
				
				table_arr.add(tm);
			}		
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
	
	public void get_orders_by_table_idRES(String jsonStr) {
		order_arr = new ArrayList<cOrderModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1);
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cOrderModel om = new cOrderModel();
				om.id = c.getString("id");
				om.food_id = c.getString("food_id");
				om.notice = c.getString("notice");
				om.dt_order = c.getString("dt_order");
				om.counts = c.getString("counts");
				
				order_arr.add(om);
			}
		} catch(JSONException e) {
			Log.e("parseRES",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseRES",e.getMessage());
		}
	}
}
