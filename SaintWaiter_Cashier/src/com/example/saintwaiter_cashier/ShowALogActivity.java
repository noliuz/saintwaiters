package com.example.saintwaiter_cashier;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ShowALogActivity extends Activity {

	String table_id = "";
	String dt_log = "";
	String table_name = "";
	String read = "";
	TextView nameTV;
	ListView ordersLV;
	ArrayAdapter<String>aaOrdersLV;
	ArrayList<String> food_name_arr = new ArrayList<String>();
	ArrayList<String> notice_arr = new ArrayList<String>();
	ArrayList<String> dt_order_arr = new ArrayList<String>();
	ArrayList<String> dt_cooked_arr = new ArrayList<String>();
	ArrayList<String> counts_arr = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_alog);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {			
		    food_name_arr = extras.getStringArrayList("food_name_arr");
		    notice_arr = extras.getStringArrayList("notice_arr");
		    dt_order_arr = extras.getStringArrayList("dt_order_arr");
		    dt_cooked_arr = extras.getStringArrayList("dt_cooked_arr");
		    counts_arr = extras.getStringArrayList("counts_arr");
		}
		//food_name_arr = (parseRES)getIntent().getSerializableExtra("food_name_arr");
		
		
		ordersLV = (ListView)findViewById(R.id.listView1);
		nameTV = (TextView)findViewById(R.id.textView1);
		nameTV.setText("  "+table_name);
		aaOrdersLV = new ArrayAdapter<String>(ShowALogActivity.this,android.R.layout.simple_list_item_1);
			
		for (int i=0;i<food_name_arr.size();i++) {		
			String dt_order = "";
			String dt_cooked = "";
			
			if (dt_order_arr.get(i).equals("null"))
				dt_order = "�����ѹ�֡";
			else 
				dt_order = convertDateTime2Time(dt_order_arr.get(i));
			
			if (dt_cooked_arr.get(i).equals("null"))
				dt_cooked = "�����ѹ�֡";
			else 
				dt_cooked = convertDateTime2Time(dt_cooked_arr.get(i));
			
			
			String tmp = " "+food_name_arr.get(i)+"-"+notice_arr.get(i)
					+" "+dt_order+" "+dt_cooked+" "+counts_arr.get(i);
			aaOrdersLV.add(tmp);
		}		
		ordersLV.setAdapter(aaOrdersLV);
	}

	public String convertDateTime2Time(String dt) {
		String tmp1[] = dt.split(" ");
		String time = tmp1[1];
		return time;		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_alog, menu);
		return true;
	}

}
