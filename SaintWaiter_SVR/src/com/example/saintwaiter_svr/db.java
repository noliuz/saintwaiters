package com.example.saintwaiter_svr;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class db extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "stsw_std.db3";
	CursorFactory cfactory;
	SQLiteDatabase sqliteDB;
	
	public db(Context context) {
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		
		try {
			String path = "/sdcard/external_sd/stwt_std.db3";
			sqliteDB = SQLiteDatabase.openDatabase(path, 
				null, SQLiteDatabase.OPEN_READWRITE);
			Log.e("db",path);
		} catch (SQLiteException e) {
			Log.e("db",e.getMessage());
		}
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion) {
		
	}
	
	public Cursor query(String sql) {
		return sqliteDB.rawQuery(sql,null); 
	}
	
	public void query_no_res(String sql) {
		sqliteDB.execSQL(sql); 
	}
}
 