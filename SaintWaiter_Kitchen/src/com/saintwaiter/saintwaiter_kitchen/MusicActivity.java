package com.saintwaiter.saintwaiter_kitchen;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;

public class MusicActivity extends Activity {

	MediaPlayer mMedia = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music);
		
		mMedia = MediaPlayer.create(this, R.raw.byorder);
		mMedia.start();
	}

	 @Override
     public boolean onTouchEvent(MotionEvent event) {
		 // TODO Auto-generated method stub
		 mMedia.stop();
		 finish();
		 return super.onTouchEvent(event);
     }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music, menu);
		return true;
	}

}
