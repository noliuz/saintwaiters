package com.example.saintwaiter_svr;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;


//Command will bundle with
// { "CMD":[{"CMD":"add_food_table"}],
//   "ARG":[{"table_id":"1","food_id":"2"}]
// }
//
public class parseCMD {
	
	db mydb;
	
	public parseCMD(Context context) {
		mydb = new db(context);
	}
	
	public static String getCMD(String jsonStr) {
		String cmd="";
		
		if (jsonStr.isEmpty() || jsonStr == null)
			return "";
		
		try {
			JSONObject jo = new JSONObject(jsonStr);
			JSONArray jarr_cmd = jo.getJSONArray("CMD");
			cmd = jarr_cmd.getJSONObject(0).getString("CMD");
		} catch(JSONException e) {
			Log.e("parseCMD",e.getMessage());
		}
		
		return cmd;
	}
	
	public String get_log_listCMD() {
		String q = "SELECT dt_log,table_id FROM order_log WHERE datetime(dt_log) >= datetime(date('now')||' 00:00:00')  GROUP BY dt_log,table_id";
		Cursor c1 = mydb.query(q);
	    ArrayList<JSONObject> jarr = new ArrayList<JSONObject>();
		if  (c1.moveToFirst() && c1 != null) {
	        do {
	        	try {        
	        		JSONObject jobj = new JSONObject();
	        		
	        		String dt_log = c1.getString(
	        				c1.getColumnIndex("dt_log"));
	        		String table_id = c1.getString(
	        				c1.getColumnIndex("table_id"));
	        			        		
	        		jobj = new JSONObject();
		            jobj.put("dt_log",dt_log);
		            jobj.put("table_id", table_id);		            
		            
		            jarr.add(jobj);
	        	} catch (JSONException e) {
	        		Log.e("parseCMD",e.getMessage());
	        	}		     		        
	        } while (c1.moveToNext());
		}
		
		//RESTYPE
		ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
		try {
			JSONObject restype_obj = new JSONObject();
			restype_obj.put("RESTYPE", "log_list");
			
			JSONArray res_arr = new JSONArray(jarr);
			JSONObject res_obj = new JSONObject();
			res_obj.put("RES", res_arr);
			
			json_al.add(restype_obj);
			json_al.add(res_obj);
			
			JSONArray json = new JSONArray(json_al);
			return json.toString();
	        
		} catch (JSONException e) {
			Log.e("parseCMD",e.getMessage());
		}
		return "";
	}
	
	public String get_report_client_detailCMD(String jsonStr) {
		String type="",name="";
		try {
			JSONObject jo = new JSONObject(jsonStr);
			JSONArray jarr = jo.getJSONArray("ARG");
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				type = c.getString("type");
				name = c.getString("name");							
			}		
		} catch(JSONException e) {
			Log.e("parseCMD",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseCMD",e.getMessage());
		}
		return type+"|"+name;
	}
	
	public void submit_foodCMD(String jsonStr) {
		
		try {
			JSONObject jo = new JSONObject(jsonStr);
			JSONArray jarr = jo.getJSONArray("ARG");
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				String table_id = c.getString("table_id");
				String food_id = c.getString("food_id");
				String notice = c.getString("notice");
				String counts = c.getString("counts");
				
				String q = "INSERT INTO orders (table_id,food_id,dt_order,notice,counts) VALUES " +
						"('"+table_id+"','"+food_id+"',datetime('now'),'"+notice+"','"+counts+"')";
				mydb.query_no_res(q);
				Log.e("parseCMDq",q);
			}		
		} catch(JSONException e) {
			Log.e("parseCMD",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("parseCMD",e.getMessage());
		}
	}
	
	public void ring_order_tabletCMD(ArrayList<SocketClientDetail> i_al_scd) {
		//find tablet_name in al_scd
	    for (SocketClientDetail scd : i_al_scd) {
	   	 	if (scd.cli_type.equals("order")) {
	   	 		 //ring to tablet
	   	 		 //RESTYPE
	 			 ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
	 			 ArrayList<JSONObject> jarr = new ArrayList<JSONObject>();
	 			 try {
	 				JSONObject restype_obj = new JSONObject();
	 				restype_obj.put("RESTYPE", "ring_order_to_serve");
	 				
	 				
	 				JSONArray res_arr = new JSONArray(jarr);
	 				JSONObject res_obj = new JSONObject();
	 				res_obj.put("RES", res_arr);
	 				
	 				json_al.add(restype_obj);
	 				json_al.add(res_obj);
	 				
	 				JSONArray json = new JSONArray(json_al);
	 				//send 	 			
                    PrintWriter out = new PrintWriter(new BufferedWriter(
           				new OutputStreamWriter(scd.cli_socket.getOutputStream())),
           				true);
	            	out.println(json.toString());	                
	 				
	 				Log.e("ring_order_to_serve_parseCMD",json.toString());
	 			 } catch (JSONException e) {
	 				Log.e("ring_order_to_serve_parseCMD",e.getMessage());
	 			 } catch (IOException e) {
	 				Log.e("ring_order_to_serve_parseCMD",e.getMessage());
	 			 }

	   	 	}
	    }		            
	}
	
	public String get_orders_for_cookCMD(Socket ssocket,ArrayList<SocketClientDetail> i_al_scd) { 
        
   	//find tablet_name in al_scd
   	String kname = "";
    for (SocketClientDetail scd : i_al_scd) {
   	 	Log.e("get_orders_for_cookCMD",scd.cli_name);
    	if (scd.cli_socket.equals(ssocket)) {
	   		 kname = scd.cli_name;
	   		 Log.e("get_orders_for_cookCMD",kname);
	   		 break;
    	}
    }		             
    if (kname.isEmpty())
   	 return "";
   	 
        //query for orders
    String q = 
   	    "SELECT food.name as food_name,orders.table_id as table_id,orders.notice as notice"+
       	",orders.counts as counts,orders.dt_order as dt_order,orders.id as order_id "+
        ",orders.dt_cooked as dt_cooked "+   
       		"FROM orders LEFT JOIN food ON orders.food_id=food.id "+
				"LEFT JOIN kitchen ON kitchen.id=food.kitchen_id "+
				"WHERE kitchen.name='"+kname+"' ORDER BY dt_order";
  
    Log.e("get_orders_for_cook",q);
    Cursor c1 = mydb.query(q);
    ArrayList<JSONObject> jarr = new ArrayList<JSONObject>();
	if  (c1.moveToFirst() && c1 != null) {
        do {
        	try {        
        		JSONObject jobj = new JSONObject();
        		
        		String food_name = c1.getString(
        				c1.getColumnIndex("food_name"));
        		String table_id = c1.getString(
        				c1.getColumnIndex("table_id"));
        		String counts = c1.getString(
        				c1.getColumnIndex("counts"));
        		String dt_order = c1.getString(
        				c1.getColumnIndex("dt_order"));
        		String order_id = c1.getString(
        				c1.getColumnIndex("order_id"));
        		String notice = c1.getString(
        				c1.getColumnIndex("notice"));
        		String dt_cooked = c1.getString(
        				c1.getColumnIndex("dt_cooked"));
        		
        		jobj = new JSONObject();
	            jobj.put("food_name",food_name);
	            jobj.put("table_id", table_id);
	            jobj.put("counts", counts);
	            jobj.put("dt_order", dt_order);
	            jobj.put("order_id", order_id);
	            jobj.put("notice", notice);
	            if (dt_cooked == null)
	            	dt_cooked = " ";
	            jobj.put("dt_cooked", dt_cooked);
	            
	            jarr.add(jobj);
        	} catch (JSONException e) {
        		Log.e("parseCMD",e.getMessage());
        	}		     		        
        } while (c1.moveToNext());
	}
    
    //RESTYPE
	ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
	try {
		JSONObject restype_obj = new JSONObject();
		restype_obj.put("RESTYPE", "orders_to_kitchen");
		
		JSONArray res_arr = new JSONArray(jarr);
		JSONObject res_obj = new JSONObject();
		res_obj.put("RES", res_arr);
		
		json_al.add(restype_obj);
		json_al.add(res_obj);
		
		JSONArray json = new JSONArray(json_al);
		return json.toString();
        
	} catch (JSONException e) {
		Log.e("parseCMD",e.getMessage());
	} 
	
	return "";	
}
	
	public void force_kitchen_check_ordersCMD(ArrayList<SocketClientDetail> i_al_scd) {
		//find kitchen name
		String q = "SELECT name FROM kitchen";
		Cursor c = mydb.query(q);
		if (c != null) {
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		             //get kitchen name
		        	 String kname = c.getString(c.getColumnIndex("name"));
		             
		        	//find socket in i_al_scd
		        	 Socket ssocket=null;		        	 
		             for (SocketClientDetail scd : i_al_scd) {
		            	 if (scd.cli_name.equals(kname) && scd.cli_type.equals("kitchen")) {
		            		 ssocket = scd.cli_socket;
		            		 Log.e("parseCMD_svr",scd.cli_name);
		            		 break;
		            	 }
		             }		             
		             if (ssocket == null)
		            	 continue;
		        	 		             
		             //RESTYPE
		 			 ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
		 		     try {
		 				JSONObject restype_obj = new JSONObject();
		 				restype_obj.put("RESTYPE", "check_orders");
		 				
		 				JSONArray res_arr = new JSONArray(jarr);
		 				JSONObject res_obj = new JSONObject();
		 				res_obj.put("RES", res_arr);
		 				
		 				json_al.add(restype_obj);
		 				json_al.add(res_obj);
		 				
		 				JSONArray json = new JSONArray(json_al);
		 				//send 	 			
                        PrintWriter out = new PrintWriter(new BufferedWriter(
	            				new OutputStreamWriter(ssocket.getOutputStream())),
	            				true);
		            	out.println(json.toString());	                
		 				
		 				Log.e("parseCMD",json.toString());
		 			 } catch (JSONException e) {
		 				Log.e("parseCMD",e.getMessage());
		 			 } catch (IOException e) {
		 				Log.e("parseCMD",e.getMessage());
		 			 }
		             
		        } while (c.moveToNext());
		    }
		}
			
	}
	
	public String get_zonesCMD() {
		JSONArray json=null;
		
		String q = "SELECT * from zones ORDER BY name";
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            String name = c.getString(c.getColumnIndex("name"));
			            String id = c.getString(c.getColumnIndex("id"));
			            
			            jobj = new JSONObject();
			            jobj.put("name",name);
			            jobj.put("id", id);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }
			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "zones");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
				
				
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
				
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}
		
			return json.toString();
		} else {
			return "";
		}
	}
	
	public String get_food_catCMD() {
		JSONArray json=null;
		
		String q = "SELECT * from food_cat ORDER BY name";
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            String name = c.getString(c.getColumnIndex("name"));
			            String id = c.getString(c.getColumnIndex("id"));
			            
			            jobj = new JSONObject();
			            jobj.put("name",name);
			            jobj.put("id", id);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }
			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "food_cat");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
				
				
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
				
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}
		
			return json.toString();
		} else {
			return "";
		}
	}
	
	public String get_foodCMD() {
		JSONArray json=null;
		
		String q = "SELECT * from food ORDER BY name";
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            String name = c.getString(c.getColumnIndex("name"));
			            String id = c.getString(c.getColumnIndex("id"));
			            String food_cat_id = c.getString(c.getColumnIndex("food_cat_id"));
			            String kitchen_id = c.getString(c.getColumnIndex("kitchen_id"));
			            String price = c.getString(c.getColumnIndex("price"));	
			            
			            jobj = new JSONObject();
			            jobj.put("name",name);
			            jobj.put("id", id);
			            jobj.put("food_cat_id", food_cat_id);
			            jobj.put("kitchen_id", kitchen_id);
			            jobj.put("price", price);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }
			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "food");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
				
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}
		
			return json.toString();
		} else {
			return "";
		}
	}
	
	public String get_tablesCMD() {
		JSONArray json = null;
		String q = "SELECT * from tables ORDER BY name";
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            String name = c.getString(c.getColumnIndex("name"));
			            String id = c.getString(c.getColumnIndex("id"));
			            String zone_id = c.getString(c.getColumnIndex("zone_id"));
			            
			            jobj = new JSONObject();
			            jobj.put("name",name);
			            jobj.put("id", id);
			            jobj.put("zone_id", zone_id);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }

			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "tables");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
							
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
				
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}

			return json.toString();
		} else {
			return "";
		}
	}
	
	public String get_orders_by_table_idCMD(String jsonStr) {
		JSONArray json = null;
		String table_id="";
		
		try {
			JSONObject jobj = new JSONObject(jsonStr);
			JSONArray jarr = jobj.getJSONArray("ARG");
			JSONObject obj2 = jarr.getJSONObject(0);
			table_id = obj2.getString("table_id");
		} catch (JSONException e) {
			Log.e("JSON",e.getMessage());
		}
		
		String q = "SELECT * from orders WHERE table_id='"+table_id+"'";
		Log.e("parseCMDq",q);
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            String id = c.getString(c.getColumnIndex("id"));
			            String food_id = c.getString(c.getColumnIndex("food_id"));
			            String notice = c.getString(c.getColumnIndex("notice"));
			            String dt_order = c.getString(c.getColumnIndex("dt_order"));
			            String dt_cooked = c.getString(c.getColumnIndex("dt_cooked"));
			            String counts = c.getString(c.getColumnIndex("counts"));
			            
			            jobj = new JSONObject();
			            jobj.put("id", id);
			            jobj.put("food_id", food_id);
			            jobj.put("notice", notice);
			            jobj.put("dt_order", dt_order);
			            jobj.put("dt_cooked", dt_cooked);
			            jobj.put("counts", counts);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }
			
			
			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "orders_by_table_id");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
							
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}
			return json.toString();
		} else {
			return "";
		}
	}
	
	public String get_orders_logCMD(String jsonStr) {
		JSONArray json = null;
		String table_id="";
		String dt_log = "";
		
		try {
			JSONObject jobj = new JSONObject(jsonStr);
			JSONArray jarr = jobj.getJSONArray("ARG");
			JSONObject obj2 = jarr.getJSONObject(0);
			table_id = obj2.getString("table_id");
			dt_log = obj2.getString("dt_log");
			
		} catch (JSONException e) {
			Log.e("JSON",e.getMessage());
		}
		
		String q = "SELECT * from order_log WHERE table_id='"+table_id+"' AND "+
				"dt_log='"+dt_log+"'";
		Log.e("parseCMDq",q);
		Cursor c = mydb.query(q);
		if (c != null) {
			JSONObject jobj;
			ArrayList<JSONObject> jarr = new ArrayList<JSONObject >();
			if  (c.moveToFirst()) {
		        do {
		        	try {
			            //String id = c.getString(c.getColumnIndex("id"));
			            String food_id = c.getString(c.getColumnIndex("food_id"));
			            String notice = c.getString(c.getColumnIndex("notice"));
			            String dt_order = c.getString(c.getColumnIndex("dt_order"));
			            String dt_cooked = c.getString(c.getColumnIndex("dt_cooked"));
			            String counts = c.getString(c.getColumnIndex("counts"));
			            
			            jobj = new JSONObject();
			            //jobj.put("id", id);
			            jobj.put("food_id", food_id);
			            jobj.put("notice", notice);
			            jobj.put("dt_order", dt_order);
			            jobj.put("dt_cooked", dt_cooked);
			            jobj.put("counts", counts);
			            jarr.add(jobj);
		        	} catch (JSONException e) {
		        		Log.e("parseCMDq",e.getMessage());
		        	}
		        }while (c.moveToNext());
		    }
						
			//RESTYPE
			ArrayList<JSONObject> json_al = new ArrayList<JSONObject>();
			try {
				JSONObject restype_obj = new JSONObject();
				restype_obj.put("RESTYPE", "orders_log");
				
				JSONArray res_arr = new JSONArray(jarr);
				JSONObject res_obj = new JSONObject();
				res_obj.put("RES", res_arr);
							
				json_al.add(restype_obj);
				json_al.add(res_obj);
				
				json = new JSONArray(json_al);
			} catch (JSONException e) {
				Log.e("parseCMD",e.getMessage());
			}
			return json.toString();
		} else {
			return "";
		}
	}
	
	public void move_to_log_by_table_idCMD(String jsonStr) {
		JSONArray json = null;
		String table_id="";
		
		try {
			JSONObject jobj = new JSONObject(jsonStr);
			JSONArray jarr = jobj.getJSONArray("ARG");
			JSONObject obj2 = jarr.getJSONObject(0);
			table_id = obj2.getString("table_id");
		} catch (JSONException e) {
			Log.e("JSON",e.getMessage());
		}
		
		String q = "SELECT * from orders WHERE table_id='"+table_id+"'";
		Log.e("parseCMDq",q);
		Cursor c = mydb.query(q);
		if (c != null) {			
			if  (c.moveToFirst()) {
		        do {		        	
		            String id = c.getString(c.getColumnIndex("id"));
		            String food_id = c.getString(c.getColumnIndex("food_id"));
		            String notice = c.getString(c.getColumnIndex("notice"));
		            String dt_order = c.getString(c.getColumnIndex("dt_order"));
		            String dt_cooked = c.getString(c.getColumnIndex("dt_cooked"));
		            String counts = c.getString(c.getColumnIndex("counts"));
		            String table_id2 = c.getString(c.getColumnIndex("table_id"));
		            
		            q = "INSERT INTO order_log (food_id,notice,dt_order,dt_cooked,"+
		            "counts,table_id,dt_log) VALUES ('"+
		            food_id+"','"+notice+"','"+dt_order+"','"+dt_cooked+"','"+
		            counts+"','"+table_id2+"',datetime('now'))";			        
		            mydb.query_no_res(q);
		        }while (c.moveToNext());
		    }
			q = "DELETE FROM orders WHERE table_id='"+table_id+"'";
			mydb.query_no_res(q);
		} else {
			return;
		}
	}
	
	public void delete_orders_by_idCMD(String jsonStr) {
		String table_id="";
		try {
			JSONObject jobj = new JSONObject(jsonStr);
			JSONArray jarr = jobj.getJSONArray("ARG");
			for (int i=0;i<jarr.length();i++) {
				JSONObject obj2 = jarr.getJSONObject(i);
				String id = obj2.getString("order_id");
				String q = "DELETE FROM orders WHERE id='"+id+"'";
				Log.e("parseCMDq",q);
				mydb.query_no_res(q);
			}
		} catch (JSONException e) {
			Log.e("JSON",e.getMessage());
		}	
	}
	
	public void send_cooked_ordersCMD(String jsonStr) {
		String table_id="";
		try {
			JSONObject jobj = new JSONObject(jsonStr);
			JSONArray jarr = jobj.getJSONArray("ARG");
			for (int i=0;i<jarr.length();i++) {
				JSONObject obj2 = jarr.getJSONObject(i);
				String id = obj2.getString("order_id");
				String q = "UPDATE orders SET dt_cooked=datetime('now') WHERE id='"+id+"'";
				Log.e("send_cooked_ordersCMD",q);
				mydb.query_no_res(q);
			}
		} catch (JSONException e) {
			Log.e("JSON",e.getMessage());
		}	
	}
	
	public void closeDB() {
		mydb.close();
	}
}
