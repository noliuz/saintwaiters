package com.saintwaiter.saintwaiter_kitchen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {
	//Constants
	public final static String tablet_name = "ByOrder";
	public final static String tablet_type = "Kitchen";
	//////////////////
	
	public static Socket socket = null;
	String SERVER_IP = "10.0.2.2";
	int SERVERPORT = 5000;
	//String SERVER_IP = "192.168.0.106";
	//int SERVERPORT = 6000;

	ClientThread ct;
	Thread tt;
	Thread cth;
	parseRES pres; 
	public Handler updateUIHandler;
	ListView orders_lv;
	TableLayout gridTL;
	ArrayAdapter<String> aa_orders_lv;
	Button updateBT;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		aa_orders_lv = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1);
		//orders_lv = (ListView)findViewById(R.id.listView1);
		updateBT = (Button)findViewById(R.id.button1);
		gridTL = (TableLayout)findViewById(R.id.grid);
		
		pres = new parseRES(this);
		
		updateUIHandler = new Handler() {
			@Override
			public void handleMessage(Message message) {
				String msg = message.getData().getString("msg").toString();
				
				Log.e("Kitchen","Handler:"+msg);
				if (msg.equals("update_orders")) {					
					Log.e("Kitchen","In handler update orders");
					updateLocalGrid();
				}  
			}
		};
		
		
		updateBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_orders_for_cook\"}]}");
            }
		});
		
		/*
		orders_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {  
			   public void onItemClick(AdapterView<?> parentView, View childView, 
			                                    int position, long id) { 
			     
			         String order_id = pres.cko_arr.get(position).order_id;
			         
			         String cmd = 
			        	 "{ \"CMD\":[{\"CMD\":\"send_cooked_orders\"}],"+
			         "\"ARG\":[{\"order_id\":\""+order_id+"\"}]}";
			         MainActivity.sendCMD(cmd);
			         Log.e("orders_lv_onClick",cmd);
			         MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_orders_for_cook\"}]}");
			   }			   
		});  
		
		orders_lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                    int pos, long id) {
                // TODO Auto-generated method stub
            	
            	aa_orders_lv.remove(aa_orders_lv.getItem(pos));
            	orders_lv.setAdapter(aa_orders_lv);
            	
            	String order_id = pres.cko_arr.get(pos).order_id;
            	
            	//create json string            	
            	try {
            		//CMD
            		ArrayList<JSONObject> ALcmd = new ArrayList<JSONObject>();
	        		JSONObject cmdObj = new JSONObject();
	        		cmdObj.put("CMD", "delete_orders_by_id");
	        		ALcmd.add(cmdObj);
	        		JSONArray JAcmd = new JSONArray(ALcmd);
	        		JSONObject JOcmd = new JSONObject();
	        		JOcmd.put("CMD", JAcmd);	        	
	        		//ARG
	        		ArrayList<JSONObject> ALarg = new ArrayList<JSONObject>();
	        		
        			JSONObject argObj = new JSONObject();
        			argObj.put("order_id", order_id);
        			
        			ALarg.add(argObj);
	        		
	        		JSONArray JAarg = new JSONArray(ALarg);
	        		JOcmd.put("ARG", JAarg);
	        		
	        		MainActivity.sendCMD(JOcmd.toString());
	        		
	        		Log.e("Order",JOcmd.toString());            	
            	} catch (JSONException e) {
            		Log.e("delete_order_id",e.getMessage());
            	}
                return true;
            }
        });*/ 
		
		//run thread		
		ct = new ClientThread();
		tt = new Thread(ct);
		tt.start();		
		
		//wait for socket connected
		while (socket == null);
		while (socket.isClosed());
		while (!socket.isConnected());
		
		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"report_client_detail\"}]"+
				  ",\"ARG\":[{\"type\":\""+tablet_type+"\",\"name\""+
				  ":\""+tablet_name+"\"}]}");
		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_zones\"}]}");
		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_tables\"}]}"); 		
				
		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_orders_for_cook\"}]}");
		
	}
	
	public void updateLocalGrid() {
		Log.e("Order","updateLocalGrid "+pres.cko_arr.size());
		gridTL.removeAllViews();
		for(int i=0; i<pres.cko_arr.size(); i++) {
	   	    TableRow row = new TableRow(this);
		
		    //find zone,table name
			String table_name="",zone_name="",zone_id="";
			for (parseRES.cTableModel tm : pres.table_arr) {
				if (tm.id.equals(pres.cko_arr.get(i).table_id)) {
					table_name = tm.name;
					zone_id = tm.zone_id;
					break;
				}
			}
			//find zone
			for (parseRES.cZoneModel zm : pres.zone_arr) {
				if (zm.id.equals(zone_id)) {
					zone_name = zm.name;
					break;
				}
			}
			
			String tmp[] = pres.cko_arr.get(i).dt_order.split(" ");
			String tmp1[] = tmp[1].split(":");
			String t_order = tmp1[0]+"."+tmp1[1]+"�.";
		  
		    TextView dataCOL = new TextView(this);
	        dataCOL.setText(" ");
	        row.addView(dataCOL);	     
	      
	        String txtrow = pres.cko_arr.get(i).food_name+"-"+
					pres.cko_arr.get(i).notice+" "+
					pres.cko_arr.get(i).counts+" "+
					zone_name+":"+table_name+" "+
					t_order;
	      
		    dataCOL = new TextView(this);
	        dataCOL.setText(txtrow);	        	        	       
	        if (!pres.cko_arr.get(i).dt_cooked.equals(" ")) 
	        	dataCOL.setTextColor(Color.RED);
	        row.addView(dataCOL);	        
	     	
	        row.setClickable(true);
	        row.setOnClickListener(new OnClickListener(){
	            @Override
	            public void onClick(View v){
	                // TODO Auto-generated method stub
	            	
	            	//check if row still RED
	            	TableRow tablerow = (TableRow)v;
	                TextView txt = (TextView) tablerow.getChildAt(1);
	            	if (txt.getTextColors().equals(Color.RED)) {
	            		return;
	            	}
	                
	            	//set row color to RED	            	
	                txt.setTextColor(Color.RED);	                	               
	                
	                //update dt_cooked
	                int index = -1;
	                	//find index of tablerow
	                for (int i=0;i< gridTL.getChildCount();i++) {
	                	if (tablerow.equals(gridTL.getChildAt(i))) {
	                		index = i;
	                		break;
	                	}
	                }
	                	//send cmd to update dt_cooked
	                String order_id = pres.cko_arr.get(index/2).order_id;			        
			        String cmd = 
			       	 "{ \"CMD\":[{\"CMD\":\"send_cooked_orders\"}],"+
			         "\"ARG\":[{\"order_id\":\""+order_id+"\"}]}";
			        MainActivity.sendCMD(cmd);			        
			       
			        //MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_orders_for_cook\"}]}");
	            }
	        });	
	        
		    gridTL.addView(row);
		  
		    //add horizontal line
		    View v = new View(this);
		    v.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
		    v.setBackgroundColor(Color.rgb(51, 51, 51));
		    gridTL.addView(v);		  
	   }
	}
	
	class ClientThread implements Runnable {
		//public static Socket socket;

		@Override
	    public void run() {
			try {
				//tt.sleep(50);
				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
	            socket = new Socket(serverAddr, SERVERPORT);
	            if (socket == null) {
	            	Log.e("Kitchen","socket is null");
	            } else {
	            	 CommunicationThread commth = new CommunicationThread(socket);
		             cth = new Thread(commth);
		             cth.start();
		             
		             /*WatchDogThread wdt = new WatchDogThread();
		     	     Thread wdtt = new Thread(wdt);
		     		 wdtt.start();*/
	            }
	            Log.e("Kitchen",socket.toString());
	        } catch (UnknownHostException e1) {
	            Log.e("Order","UnknownHost Exception.");
	        } catch (IOException e1) {
	            Log.e("Order","IO Exception.",e1);
	        } /*catch (InterruptedException e1) {
	        	Log.e("Order",e1.getMessage());
	        }*/

	    }
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        try {
	        	socket.close();
	        	cth.interrupt();
	        	tt.interrupt();
	        } catch (IOException e) {
	        	Log.e("Kitchen","Socket close.");
	        }
	    	finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	/*
	public void updateOrdersLV() {
		Log.e("Kitchen","updateOrdersLV"+pres.cko_arr.size());
			
		aa_orders_lv.clear();
		for(int i=0; i<pres.cko_arr.size(); i++) {
			//find zone,table name
			String table_name="",zone_name="",zone_id="";
			for (parseRES.cTableModel tm : pres.table_arr) {
				if (tm.id.equals(pres.cko_arr.get(i).table_id)) {
					table_name = tm.name;
					zone_id = tm.zone_id;
					break;
				}
			}
			//find zone
			for (parseRES.cZoneModel zm : pres.zone_arr) {
				if (zm.id.equals(zone_id)) {
					zone_name = zm.name;
					break;
				}
			}
			
			String tmp[] = pres.cko_arr.get(i).dt_order.split(" ");
			String tmp1[] = tmp[1].split(":");
			String t_order = tmp1[0]+"."+tmp1[1]+"�.";
			
			String row = pres.cko_arr.get(i).food_name+"-"+
					pres.cko_arr.get(i).notice+" "+
					pres.cko_arr.get(i).counts+" "+
					zone_name+":"+table_name+" "+
					t_order;
			
			
			
			Log.e("Kitchen",row);
			aa_orders_lv.add(row);
			
			
		}
		
		orders_lv.setAdapter(aa_orders_lv);
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
			
	class CommunicationThread implements Runnable {
        private Socket clientSocket;
        private BufferedReader input;

        public CommunicationThread(Socket sclientSocket) {
            clientSocket = sclientSocket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
            	Log.e("Kitchen commThread",e.getMessage());
            	try {
            		this.clientSocket.close();
            	} catch (IOException e1) {
            		Log.e("Kitchen commThread",e.getMessage());
            	}
            }
            /*
        	try {
            	cth.sleep(1000);
            } catch (InterruptedException e) {
            	Log.e("Order",e.getMessage());
            }*/
        }
        
        public void run() {
            
            
        	if (!clientSocket.isConnected()) {
            	Log.e("Kitchen","not connected");
            	return;
            }
            
        	while (!Thread.currentThread().isInterrupted() ) {
        		try {
                    String read = "";                    
                    
                   	read = input.readLine();
                   	Log.e("Kitchen",read);
                    if (!read.isEmpty()) {
                    	String restype = parseRES.getRESTYPE(read);
                    	Log.e("Kitchen","restype:"+restype);
                    	
                    	if (restype.equals("zones")) {
                    		pres.get_zonesRES(read);	
                    		pres.msg = "update_zones";
                    		
                    		/*
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_zones");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		*/
                    		Log.e("Kitchen",pres.zone_arr.get(0).name);
                    	} else if (restype.equals("tables")) {
                    		pres.get_tablesRES(read);
                    		pres.msg = "update_tables";
                    		
                    		/*
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_tables");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);                    		
                    		*/
                    		Log.e("Kitchen",pres.table_arr.get(0).name);
                    		
                    	} else if (restype.equals("orders_to_kitchen")) {                    		
                    		pres.get_orders_for_cookRES(read);
                    		
                    		//send messasge to update UI
                    		Message msg = new Message();
                    		Bundle bd = new Bundle();
                    		bd.putString("msg","update_orders");
                    		msg.setData(bd);
                    		updateUIHandler.sendMessage(msg);
                    		
                    		//check if have food not cooked yet
                    		for (parseRES.cKitchenOrderModel kom:pres.cko_arr) {
                    			if (kom.dt_cooked.equals(" ")) {
                    				Intent intent = new Intent(MainActivity.this, MusicActivity.class);
                                    startActivity(intent);
                                    break;
                    			}
                    		}
                    		                    		                    		
                    	} else if (restype.equals("check_orders")) {                    		
                    		MainActivity.sendCMD("{\"CMD\":[{\"CMD\":\"get_orders_for_cook\"}]}");
                    	} 
                    	
                    }
               } catch (IOException e) {
                	Log.e("Kitchen",e.getMessage());
               } catch (NullPointerException e) {
            	  
               }
            } 
        }  
	}
	
	class WatchDogThread implements Runnable {
		public WatchDogThread() {
		
		}
		
		public void run() {
		
			if (!socket.isConnected()) {
				cth.interrupt();
				tt.interrupt();
			}
		}
	}
	
	public static void sendCMD(String cmdJson) {
		Log.e("sendCMD",cmdJson);
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(socket.getOutputStream())),
					true); 
			out.println(cmdJson);
		} catch (IOException e) {
			Log.e("sendCMD",e.getMessage());
		}
	}
	
	
	
}
