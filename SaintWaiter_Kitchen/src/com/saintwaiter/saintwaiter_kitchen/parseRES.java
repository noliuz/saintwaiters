package com.saintwaiter.saintwaiter_kitchen;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class parseRES {
	
	public class cZoneModel {
		String id;
		String name;
	}

	public class cTableModel {
		String id;
		String name;
		String zone_id;
	}

	public class cKitchenOrderModel {
		String order_id;
		String food_name;
		String notice;
		String dt_order;
		String counts;
		String table_id;
		String dt_cooked;
	}
	
	public class cFoodCatModel {
		String id;
		String name;
	}
	
	public class cFoodModel {
		String id;
		String food_cat_id;
		String kitchen_id;
		String name;
		String price;
	}
	
	Context context;
	
	public ArrayList<cZoneModel> zone_arr= new ArrayList<cZoneModel>(); 
	public ArrayList<cTableModel> table_arr = new ArrayList<cTableModel>();
	public ArrayList<cKitchenOrderModel> cko_arr = new ArrayList<cKitchenOrderModel>();
	
	
	public String msg = "";
	
	
	public parseRES(Context scontext) {
		context = scontext;
	}
	
	public static String getRESTYPE(String jsonStr) {
		Log.e("getRESType",jsonStr);
		String restype="";
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(0);
			restype = jo.getString("RESTYPE");	
		} catch(JSONException e) {
			Log.e("getRESType",e.getMessage());
		}		
		return restype;
	}
	
	public void get_orders_for_cookRES(String jsonStr) {
		
		Log.e("get_orders_for_cookRES",jsonStr);
		cko_arr = new ArrayList<cKitchenOrderModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cKitchenOrderModel kom = new cKitchenOrderModel();
				kom.table_id = c.getString("table_id");
				kom.food_name = c.getString("food_name");
				kom.dt_order = c.getString("dt_order");
				kom.counts = c.getString("counts");
				kom.notice = c.getString("notice");
				kom.order_id = c.getString("order_id");
				kom.dt_cooked = c.getString("dt_cooked");
				
				Log.e("parseRES",kom.dt_order);
				
				cko_arr.add(kom);
			}		
		} catch(JSONException e) {
			Log.e("Kitchen get orders for cook",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("Kitchen get orders for cook",e.getMessage());
		}
	}	
	
	public void get_zonesRES(String jsonStr) {
		zone_arr = new ArrayList<cZoneModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1); 
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cZoneModel zm = new cZoneModel();
				zm.id = c.getString("id");
				zm.name = c.getString("name");
				zone_arr.add(zm);
			}		
		} catch(JSONException e) {
			Log.e("Kitchen",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("Kitchen",e.getMessage());
		}
	}
	
	public void get_tablesRES(String jsonStr) {
		table_arr = new ArrayList<cTableModel>();
		
		try {
			JSONArray ja = new JSONArray(jsonStr);
			JSONObject jo = ja.getJSONObject(1);
			JSONArray jarr = jo.getJSONArray("RES");			
			for (int i=0;i<jarr.length();i++) {
				JSONObject c = jarr.getJSONObject(i);
				cTableModel tm = new cTableModel();
				tm.id = c.getString("id");
				tm.name = c.getString("name");
				tm.zone_id = c.getString("zone_id");
				
				table_arr.add(tm);
			}		
		} catch(JSONException e) {
			Log.e("Kitchen",e.getMessage());
		} catch(SQLiteException e) {
			Log.e("Kitchen",e.getMessage());
		}
	}
}
